import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class JwtService {

  private authInfo = {};

  constructor(
    public router: Router
  ) {}

  private getAuthInfo(){
    if (sessionStorage.getItem('auth')) {
      this.authInfo = JSON.parse(sessionStorage.getItem('auth'));
      if(!this.authInfo || Object.keys(this.authInfo).length == 0) {
        sessionStorage.clear();
        this.router.navigate(["/login"]);
      }
    }else{
      this.router.navigate(["/login"]);
    }
  }

  public getToken() {

    this.getAuthInfo();
    
    return  this.authInfo['token_type'] + " " + this.authInfo['accessToken'];

  }

  public getRefreshToken() {

    this.getAuthInfo();

    return  this.authInfo['refreshToken'];
  }
}
