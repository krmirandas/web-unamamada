import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { ValidatorsFormService } from "@shared/services/validators-form/validators-form.service";
import { Auth } from "../auth.model";
import { Router, ActivatedRoute,Params } from '@angular/router';


@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"]
})
export class ResetPasswordComponent implements OnInit {

  formResetPwd = this.formBuilder.group({
    email: ["", [Validators.required, ValidatorsFormService.emailValidator]],
    password: ["", Validators.required],
    password_confirmation: ["", Validators.required]
  },
    {
      validator: ValidatorsFormService.passwordMatchValidator,
    },
  );
  public success: Boolean = false;
  public error: Boolean = false;
  private message: String = "Error";
  private secret:any;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,

  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      params => {
        if(!params.hasOwnProperty('secret')){
        	this.router.navigate(['login']);
        }else{
        	this.secret = params.secret;
        }
      }
    );
  }


  reset() {
    if (this.formResetPwd.valid) {
      let reset = new Auth(
        this.formResetPwd.controls.email.value,
        this.formResetPwd.controls.password.value,
        this.formResetPwd.controls.password_confirmation.value,
      );
      this.authService.resetPassword(reset,this.secret).subscribe(
        data => {
          this.success = true;
        }, error => {
          this.error = true;
          console.log(error)
          this.throwError(error);
        }
      );
    }
  }

  throwError(error) {
    console.log("Error inesperado")
  }

  validateToken() {
    let token = this.route.snapshot.paramMap.get("token");
    return true;
  }
}
