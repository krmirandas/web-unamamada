import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ResetPasswordRoutingModule } from "./reset-password-routing.module";
import { ResetPasswordComponent } from "./reset-password.component";
import { ReactiveFormsModule } from "@angular/forms";
import { ControlMessagesModule } from "@shared/components/control-messages/control-messages.module";

@NgModule({
  declarations: [ResetPasswordComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ControlMessagesModule,
    ResetPasswordRoutingModule
  ]
})
export class ResetPasswordModule {}
