import { Injectable } from "@angular/core";
import { environment } from '@environment/environment';
import { CustomHttp } from "@shared/helpers/custom/http";
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';
import { JwtService } from '@modules/auth/jwt/jwt.service';
import { NotifierService } from "angular-notifier";


@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(
    public http: HttpClient,
    public customHttp: CustomHttp,
    private jwt: JwtService,
    private notifier: NotifierService
  ) { }

  isAuthenticated(): boolean {
    return sessionStorage.getItem("auth") != null;
  }

  isTokenExpired(): boolean {
    return false;
  }

  deleteToken() {
    sessionStorage.removeItem('auth');
  }

  loginWithParams(params) {
    return this.http.post(environment.API.SESSION_URL, params);
  }

  refreshToken() {
    let refresh_token = this.jwt.getRefreshToken();
    const params = {
      refresh_token: refresh_token,
      grant_type: "refresh_token"
    };
    return this.http.post(environment.API.SESSION_URL, params)
      .toPromise()
      .then(
        data => {
          sessionStorage.clear();
          sessionStorage.setItem("auth", JSON.stringify(data));
          this.notifier.notify(
            "success",
            "Conexión restablecida"
          );
          this.notifier.notify(
            "warning",
            "Su solicitud pudo haberse perdido, repítala o recargue la página"
          );
        },
        error => {}
      );
  }

  recoveryPassword(params) {
    return this.http.post(environment.API.PASSWORD_URL, params);
  }

  resetPassword(params, secret) {
    let headers = new  HttpHeaders();
    return this.http.put(environment.API.RESET_URL, params, {});
  }

  logOut() {
    return this.customHttp.delete(environment.API.SESSION_URL);
  }
}
