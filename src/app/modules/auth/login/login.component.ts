import { AuthService } from "./../auth.service";
import { Component, OnInit, Inject, Optional } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ValidatorsFormService } from "@shared/services/validators-form/validators-form.service";
import { UUID } from "angular2-uuid";
import { Router } from "@angular/router";
import { Auth } from "../auth.model";
import { MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {
  private errorLogin: boolean = false;
  public isLoading = false;
  public device_uuid = UUID.UUID();
  formLogin = this.formBuilder.group({
    email: ["", [Validators.required, ValidatorsFormService.emailValidator]],
    password: ["", Validators.required],
    remember: [true]
  });
  login_form_submitted = false;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public dialogMode: boolean = false,
    @Optional() public dialogRef: MatDialogRef<LoginComponent>,
    public formBuilder: FormBuilder,
    public router: Router,
    public authService: AuthService
  ) {}

  ngOnInit() {
  }

  login() {
    this.login_form_submitted = true;
    this.errorLogin = false;
    if (this.formLogin.valid) {
      this.isLoading = true;
      let login_data = new Auth(
        this.formLogin.controls.email.value,
        this.formLogin.controls.password.value,
        "password"
      );
      this.authService.loginWithParams(login_data).subscribe(
        data => {
          sessionStorage.setItem("auth", JSON.stringify(data));
          if(this.dialogMode) {
            this.dialogRef.close();
          } else {
            this.router.navigate(["/dashboard/home"]);
          }
        },
        error => {
          this.throwError(error);
          this.router.navigate(["/login"]);
        }
      );
    }
  }

  throwError(error) {
    this.isLoading = false;
  }
}
