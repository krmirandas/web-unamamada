import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RecoveryPasswordRoutingModule } from "./recovery-password-routing.module";
import { RecoveryPasswordComponent } from "./recovery-password.component";
import { ControlMessagesModule } from "@shared/components/control-messages/control-messages.module";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [RecoveryPasswordComponent],
  imports: [
    CommonModule,
    RecoveryPasswordRoutingModule,
    ReactiveFormsModule,
    ControlMessagesModule
  ]
})
export class RecoveryPasswordModule {}
