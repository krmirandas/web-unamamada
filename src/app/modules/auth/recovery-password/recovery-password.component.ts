import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { ValidatorsFormService } from "@shared/services/validators-form/validators-form.service";
import { Auth } from "../auth.model";

@Component({
  selector: "app-recovery-password",
  templateUrl: "./recovery-password.component.html",
  styleUrls: ["./recovery-password.component.scss"],
  providers: [AuthService]
})
export class RecoveryPasswordComponent implements OnInit {
  formRecoveryPwd = this.formBuilder.group({
    email: ["", [Validators.required, ValidatorsFormService.emailValidator]]
  });
  success: boolean = false;
  isLoading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit() {}

  recovery() {
    if (this.formRecoveryPwd.valid) {
      this.isLoading = true;
      let email_pwd = new Auth(
        this.formRecoveryPwd.controls.email.value,
        "",
        ""
      );
      this.authService.recoveryPassword(email_pwd).subscribe(
        data => {
          this.success = true;
        },
        error => {
          this.throwError(error);
        }
      );
    }
  }

  throwError(error) {
    this.isLoading = false;
  }
}
