export class Auth {
  public email?: string = "";
  public password?: string = "";
  public grant_type: string = "";


  constructor(email: string, password: string, grant_type: string) {
    this.email = email;
    this.password = password;
    this.grant_type = grant_type;
  }
}
