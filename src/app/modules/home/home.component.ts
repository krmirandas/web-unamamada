import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  cols: number = 3;
  cardMaxRows: number = 2;

  updateCols(val: any): void {
    val = Number(val);
    this.cols = isNaN(val) ? 3 : val;
  }

  updateCardMaxRows(val: any): void {
    val = Number(val);
    this.cardMaxRows = isNaN(val) ? 3 : val;
  }

  orderChanged(e: any): void {
    console.log("Order changed: ", e);
  }

  constructor() {}

  ngOnInit() {}
}
