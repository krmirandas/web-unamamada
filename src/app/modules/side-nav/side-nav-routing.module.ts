import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SideNavComponent } from "./side-nav.component";
import { AuthGuard } from '@modules/auth/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: SideNavComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "dashboard/home",
        loadChildren: "@modules/home/home.module#HomeModule"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SideNavRoutingModule {}
