import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material/list";
import { SideNavComponent } from "./side-nav.component";
import { SideNavRoutingModule } from "./side-nav-routing.module";

@NgModule({
  declarations: [SideNavComponent],
  imports: [
    CommonModule,
    SideNavRoutingModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule
  ],
  exports: []
})
export class SideNavModule {}
