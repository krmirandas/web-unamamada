import { AuthService } from './../auth/auth.service';
import { ChangeDetectorRef, Component, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { MediaMatcher } from "@angular/cdk/layout";
import { Router, NavigationEnd } from '@angular/router';
import { environment } from '@environment/environment';

@Component({
  selector: "app-side-nav",
  templateUrl: "./side-nav.component.html",
  styleUrls: ["./side-nav.component.scss"],
  providers: [AuthService],
})
export class SideNavComponent implements OnDestroy {
  public blogURL: string = environment.EXTERNAL_URL.BLOG;
  user;
  year = new Date().getFullYear();


  public mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private authService: AuthService,
    private router: Router) {
    this.mobileQuery = media.matchMedia("(max-width: 768px)");
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.router.events.subscribe(() => {
      document.querySelector('#container').scrollTop = 0;
    });
  }

  ngOnInit() {
    this.user = "";//JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  goToBlog(): void {
    window.open(this.blogURL, "_blank");
  }

  logOut() {
    this.authService.logOut().subscribe(
      response => {
        localStorage.clear();
        this.authService.deleteToken();
        this.router.navigate(['/login']);
      },
      error => {
        console.log(error);
      })
  }

}
