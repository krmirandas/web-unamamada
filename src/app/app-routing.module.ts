import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "login",
    loadChildren: "@modules/auth/login/login.module#LoginModule"
  },
  {
    path: "recovery-password",
    loadChildren: "@modules/auth/recovery-password/recovery-password.module#RecoveryPasswordModule"
  },
  {
    path: "change_password",
    loadChildren: "@modules/auth/reset-password/reset-password.module#ResetPasswordModule"
  },
  {
    path: "dashboard",
    loadChildren: "@modules/side-nav/side-nav.module#SideNavModule"
  },
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "**", redirectTo: "/login" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
