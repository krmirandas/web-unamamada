import { TestBed } from '@angular/core/testing';

import { ValidatorsFormService } from './validators-form.service';

describe('ValidatorsFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidatorsFormService = TestBed.get(ValidatorsFormService);
    expect(service).toBeTruthy();
  });
});
