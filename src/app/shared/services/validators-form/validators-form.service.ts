import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Injectable({
  providedIn: "root"
})
export class ValidatorsFormService {
  static esp = {
    required: "Este campo es obligatorio",
    invalidUrl: "Introduzca un link válido",
    invalidEmailAddress: "Introduzca un correo electrónico válido",
    invalidNumberPhone: "Número teléfonico inválido",
    invalidMatchPassword: "Las contraseñas no coinciden",
    invalidPassword: "Contraseña invalida. La contraseña debe tener al menos 8 caracteres de longitud y un número.",
  };

  static eng = {
    required: "This field is required",
    invalidUrl: "Enter a valid link",
    invalidEmailAddress: "Enter a valid email",
    invalidNumberPhone: "Invalid phone number",
    invalidMatchPassword: "Passwords are not match",
    invalidPassword: "Invalid password. Password must be at least 8 characters long, and contain a number.",
  };

  static getValidatorErrorMessage(
    validatorName: string,
    validatorValue?: any,
    language: string = "esp"
  ) {
    let config = {
      required: this.msg(language, "required"),
      invalidUrl: this.msg(language, "invalidUrl"),
      invalidEmailAddress: this.msg(language, "invalidEmailAddress"),
      invalidPassword: this.msg(language, "invalidPassword"),
      minlength: `Longitud mínima ${validatorValue.requiredLength}`,
      invalidNumberPhone: this.msg(language, "invalidNumberPhone"),
      invalidMatchPassword: this.msg(language, "invalidMatchPassword"),
    };

    return config[validatorName];
  }

  static urlValidator(control) {
    if (
      control.value.match(
        /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/
      )
    ) {
      return null;
    } else {
      return { invalidUrl: true };
    }
  }

  static emailValidator(control) {
    // RFC 2822 compliant regex
    if (
      control.value.match(
        /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
      )
    ) {
      return null;
    } else {
      return { invalidEmailAddress: true };
    }
  }

  static passwordValidator(control) {
    // {6,100}           - Assert password is between 8 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (control.value.match(/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/)) {
      return null;
    } else {
      return { invalidPassword: true };
    }
  }

  static passwordLengthValidator(group: FormGroup) {
    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (group.controls["password"].value.length > 8|| group.controls["password_confirmation"].value.length > 8) {
      return null;
    } else {
      return { invalidPassword: true };
    }
  }

  static passwordMatchValidator(group: FormGroup) {
    if (
      group.controls["password_confirmation"].value ===
      group.controls["password"].value
    ) {
      return null;
    } else {
      return { invalidMatchPassword: true };
    }
  }

  static phoneNumberValidator(control) {
    if (control.value.match(/[1-9][0-9]{9,14}/)) {
      return null;
    } else {
      return { invalidNumberPhone: true };
    }
  }

  static msg(lang, property: string) {
    let lblMsg = {};
    switch (lang) {
      case "esp":
        lblMsg = this.esp;
        break;
      case "eng":
        lblMsg = this.eng;
        break;
      default:
        break;
    }
    return lblMsg[property];
  }
}
