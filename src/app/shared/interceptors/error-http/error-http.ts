import { Injectable } from "@angular/core";
import { AuthService } from "@modules/auth/auth.service";
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { MatDialog } from "@angular/material";
import { LoginComponent } from "@modules/auth/login/login.component";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { NotifierService } from "angular-notifier";

@Injectable()
export class ErrorHttp implements HttpInterceptor {
  constructor(
    private notifier: NotifierService,
    private authService: AuthService,
    private dialog: MatDialog
    ) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // Todo sale bien
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        this.errorCase(error);
        return throwError(error);
      })
    );
  }

  private errorCase(response) {
    let title = '';
    let message = '';
    
    if (response.error && response.error.errors) {
      for (const error of response.error.errors) {
        if (error.fields) {
          message += error.fields + ': ' + error.message + '\n';
        }
      }
    } else if (response.error && response.error.message) {
      message += response.error.message;
    }
    if (response.status === 400) {
      title += 'Solicitud incompleta o inválida: 400';
      this.showSpecificNotification("error", message, title);
    }else if(response.status === 401){
      if(response.error && response.error.code == 4013) {
        let message = "Se ha perdido la conexión, intentando reconectar.";
        this.showSpecificNotification("info", message);
        this.authService.refreshToken().catch(
          error => {
            sessionStorage.clear();
            this.notifier.notify(
              "error",
              "No fue posible reconectar, inicie sesión nuevamente"
            );
            this.dialog.open(LoginComponent, {
              data: true,
              width: "100%",
              maxWidth: "100%",
              height: "100%",
              maxHeight: "100%"
            });
          }
        );
      } else {
        this.showSpecificNotification("error", message);
        sessionStorage.clear();
        this.dialog.open(LoginComponent, {
          data: true,
          width: "100%",
          maxWidth: "100%",
          height: "100%",
          maxHeight: "100%"
        });
      }
    } else if (response.status === 404) {
      let message = "Recurso no disponible";
      this.showSpecificNotification("error", message);
    } else if (response.status === 500) {
      title += 'Hubo un error con el servidor: 500';
      this.showSpecificNotification("error", message, title);
    } else if (response.status === 410) {
      title += 'Inicia sesión';
      this.showSpecificNotification("error", message, title);
      // this.router.navigate(['/']);
    } else {
      title += 'Hubo un error que hace falta cachar';
      // this.toastr.errorToastr(message, title);
      this.showSpecificNotification("error", message, title);
    }
  }

  public throwError(errors){
    console.log(errors);
    let message = '';
    var tmp = errors.errors;
    console.log(errors);
    if(typeof tmp == "object"){
      for(let attr in tmp){
        console.log(attr);
        message+= tmp[attr]+'\n';
      }
    }else{
      message = tmp;
    }
    // this.openErrorAlert(message);
    this.showSpecificNotification("error", message, null);
  }

  private showSpecificNotification(
    type: string,
    message: string,
    id?: string
  ): void {
    this.notifier.show({
      id,
      message,
      type
    });
  }
}
