import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders
} from "@angular/common/http";
import { JwtService } from "@modules/auth/jwt/jwt.service";

@Injectable({
  providedIn: "root"
})
export class CustomHttp {
  constructor(private http: HttpClient, private jwt: JwtService) {}

  createAuthorizationHeader() {
    let headers = new HttpHeaders();
    return headers.set("Authorization", this.jwt.getToken());
  }

  get<T>(url) {
    let headers = this.createAuthorizationHeader();
    return this.http.get<T>(url, {
      headers: headers
    });
  }

  post<T>(url, data) {
    let headers = this.createAuthorizationHeader();
    return this.http.post<T>(url, data, {
      headers: headers
    });
  }

  delete<T>(url) {
    let headers = this.createAuthorizationHeader();
    return this.http.delete<T>(url, {
      headers: headers
    });
  }

  put<T>(url, data) {
    let headers = this.createAuthorizationHeader();
    return this.http.put<T>(url, data, {
      headers: headers
    });
  }

  patch<T>(url, data) {
    let headers = this.createAuthorizationHeader();
    return this.http.patch<T>(url, data, {
      headers: headers
    });
  }
}
