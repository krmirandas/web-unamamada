import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
  
var sc = document.createElement("script");
sc.setAttribute("src", 
  "https://maps.googleapis.com/maps/api/js?key="
  + environment.GMAPS_KEY                
  + "&libraries=places&language=es"
);
sc.setAttribute("type", "text/javascript");
document.head.appendChild(sc);