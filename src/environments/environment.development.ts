// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const BASE_URL = 'https://10.0.0.224';
export const GOOGLE_KEY = 'AIzaSyA7EjjQ__wYa7FewM16QDl8Z921bThxA5w';

export const environment = {
  production: true,
  version: '0.0.1',
  GMAPS_KEY: GOOGLE_KEY,
  YOUTUBE: {
    REGEX: "^https:\/\/(?:www\.)?youtube.com\/watch\\?v=[A-z0-9\-_]+",
    WATCH_URL: "https://www.youtube.com/watch?v=",
    EMBED_URL: "https://www.youtube.com/embed/",
    IMG_URL: "https://img.youtube.com/vi/VIDEO_ID/hqdefault.jpg"
  },
  EXTERNAL_URL: {
    BLOG: "https://www.blogger.com/about/?r=1-null_user",
    REVERSE_GEO: "https://maps.googleapis.com/maps/api/geocode/json?language=es-419&result_type=street_address&location_type=ROOFTOP&key=" + GOOGLE_KEY + "&"
  },
  API: {
    URL_BASE: '',
    SESSION_URL: BASE_URL + '/admins/session',
    PASSWORD_URL: BASE_URL + '/admins/password',
    CHANGE_URL: BASE_URL + '/admins/change_password',
    RESET_URL: BASE_URL + '/admins/password',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
